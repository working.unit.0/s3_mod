resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.bucket}"
  region = "${var.region}"
  acl    = "${var.acl}"

  cors_rule = "${var.cors_rule}"
  website   = "${var.website}"

  force_destroy = "${var.force_destroy}"

  lifecycle_rule = "${var.lifecycle_rule}"

  versioning {
    enabled = "${var.versioning}"
  }

  request_payer                        = "${var.request_payer}"
  replication_configuration            = "${var.replication_configuration}"
  server_side_encryption_configuration = "${var.server_side_encryption_configuration}"

  tags = "${var.tags}"
}

data "template_file" "static_hosting_policy" {
  template = "${file("${path.module}/templates/static_hosting_policy.tpl")}"

  vars {
    bucket_name = "${var.bucket}"
  }
}

resource "null_resource" "local" {
  triggers {
    template = "${data.template_file.static_hosting_policy.rendered}"
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.static_hosting_policy.rendered}\""
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = "${var.policy != "" ? var.bucket : var.bucket}"
  policy = "${var.static_hosting ? data.template_file.static_hosting_policy.rendered : var.policy}"
}
